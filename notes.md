---
title: Notes | Aleph
---

# Notes

  * Add a `pattern value` that can be used to 'compile' a form into a pattern
    matcher. I.e. move logic from `recognizer` into symbols themselves.
  * https://github.com/ALTree/bigfloat

## Stream-capable

Aleph should be capable of processing data streams. Specifically, Particle could
use Aleph to evaluate things, but this is practical only if Aleph can process
streams.

## Compile

For "Compilation", the "compiled" function should have metadata attached that
says, "this function outputs data matching this pattern". So when compiling
function A, if A 'calls' B with known data types, then the 'compiled' version
might be able to hook directly into the correct implementation.

## Custom Modules

Custom modules should be able to be added directly into the kernel. They should
also be able to be dynamically added, via `link`. How this would work: You run a
module as its own 'kernel', separately. It listens for packets using
`link.Connection`, and processes them however it wants. You can tell a kernel,
"there's a module at this socket address," and it will connect, query what
functions are available, and proxy those to the module/kernel.