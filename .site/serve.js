let options = require('./options'),
    express = require('express'),
    path = require('path')

require('firesmith').watch(options.config, options.alterations).catch(err => {
    console.error(err, err.stack)
    process.exit(1)
})

const paths = ['dist', 'www']
const app = express()
paths.map(x => path.join(__dirname, x))
     .map(x => app.use(express.static(x)))
// app.use('/assets', express.static(path.join(__dirname, '..', 'assets')))
app.listen(1337)