let options = require('./options')

require('firesmith')(options.config, options.alterations).catch(err => {
    console.error(err, err.stack)
    process.exit(1)
})