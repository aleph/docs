const path = require('path')
const Recipe = require('firesmith/pipe/recipe')

const {
    DefaultsPostProcessMiddleware,
    ContentPreRenderMiddleware,
    ContentRenderMarkdownMiddleware
} = require('./lib/pipeline')

const config = {
    src: path.join(__dirname, '..'),
    dist: path.join(__dirname, 'dist'),
    layouts: path.join(__dirname, 'layouts'),

    ignore: [
        'README.md',
    ]
}

const alterations = {
    content: {
        render: {
            render(pipeline, config, original) {
                new Recipe.Sequence({
                    pre: ContentPreRenderMiddleware,
                    render: original
                }).apply(pipeline, config, {
                    render: {
                        markdown: ContentRenderMarkdownMiddleware
                    }
                })
            }
        }
    },
    defaults: {
        process: {
            processYaml(pipeline, config, original) {
                new Recipe.Sequence([
                    original,
                    DefaultsPostProcessMiddleware
                ]).apply(pipeline, config)
            }
        }
    }
}

module.exports = { config, alterations }