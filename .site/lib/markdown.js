const MarkdownIt = require('markdown-it')
const hljs = require('highlight.js')
const cheerio = require('cheerio')

const defaultRenders = {}

const toc = Symbol()

const md = new MarkdownIt({
    html: true,
    xhtmlOut: true,
    linkify: true,
    highlight: codeHighlighting,
}).use(markdownPlugin)

module.exports = { md, toc }

function markdownPlugin(md, opts) {
    Object.assign(defaultRenders, {
        link_close: md.renderer.rules.link_close || renderToken
    })

    md.core.ruler.push('macro', parseMacro)
    md.core.ruler.push('links', parseLinks)
    md.core.ruler.push('anchor', parseAnchor)
    md.renderer.rules.link_close = renderLinkClose
    md.renderer.rules.macro = renderMacro
}

function parseMacro(state) {
    const { Token } = state

    for (const block of state.tokens) {
        if (block.type != 'inline') continue

        // scan from end, so that adding tokens doesn't break things
        for (let i = block.children.length - 1; i >= 0; i--) {
            const token = block.children[i]
            if (token.type != 'text') continue

            const re = /{\s*([-a-z0-9\.]+)\s*:([^}]+)}/ig
            const matches = []
            while (true) {
                const match = re.exec(token.content)
                if (match) matches.push(match)
                else break
            }

            if (!matches.length) continue

            let index = 0
            const tokens = []
            for (const match of matches) {
                if (match.index > index) {
                    const text = new Token('text', '', 0)
                    text.content = token.content.substring(index, match.index)
                    tokens.push(text)
                }

                const [content, name, args] = match
                const macro = new Token('macro', '', 0)
                macro.content = content
                macro.name = name
                macro.args = args
                tokens.push(macro)

                index = match.index + content.length
            }

            if (index < token.content.length) {
                const text = new Token('text', '', 0)
                text.content = token.content.substring(index)
                tokens.push(text)
            }

            block.children.splice(i, 1, ...tokens)
        }
    }
}

function parseLinks(state) {
    for (const block of state.tokens) {
        if (block.type != 'inline') continue

        for (const token of block.children) {
            if (token.type != 'link_open') continue

            let href = token.attrGet('href')
            if (!href) continue

            if (/^(\w+:)?\/\//.test(href)) {
                token.attrPush(['target', '_blank'])
                token.attrPush(['class', 'external'])
                continue
            }

            if (href.endsWith('.md')) {
                token.attrSet('href', href.substring(0, href.length - 3) + '.html')
                continue
            }
        }
    }
}

function parseAnchor(state) {
    const slugs = {}
    const tokens = state.tokens.map((x, i) => ({ token: x, index: i }))

    for (const { token, index } of tokens) {
        if (token.type !== 'heading_open') continue

        const level = Number(token.tag.substring(1))
        const data = token[toc] = { level }
        if (isNaN(level) || level > 3) continue

        const close = tokens.slice(index + 1).find(x => x.token.type === 'heading_close')
        const content = tokens.slice(index + 1, close.index).map(x => x.token)
        const html = md.renderer.render(content, md.options, state.env)

        const $ = cheerio.load(html)
        const tocText = $($._root).contents().slice(0, 1).text()
        const slug = $($._root).text().replace(/[^a-z0-9]+/ig, '-').toLowerCase()
        const anchor = uniqueSlug(slug, slugs)

        Object.assign(data, { tocText, anchor })
        token.attrPush(['id', anchor])
    }
}

function renderLinkClose(tokens, idx, options, env, self) {
    const tag = defaultRenders.link_close(tokens, idx, options, env, self)
    const open = findLinkOpen(tokens, idx)
    if (!open) throw new Error('missing link_open for link_close')

    if (open.attrGet('class') != 'external') return tag

    return `<i class="material-icons">launch</i>${tag}`
}

function renderMacro(tokens, idx, options, env, self) {
    const token = tokens[idx]

    switch (token.name) {
    case 'fa':
        return `<i class="fa fa-${token.args.trim()}"></i>`

    case 'mi':
        return `<i class="material-icons">${token.args.trim().replace('-', '_')}</i>`

    default:
        return md.renderer.rules.text(tokens, idx, options, env, self)
    }
}

function codeHighlighting(str, lang) {
    if (!lang)
        return `<pre><code>${md.utils.escapeHtml(str)}</code></pre>`

    if (hljs.getLanguage(lang))
      try {
        return `<pre><code class="hljs">${hljs.highlight(lang, str, true).value}</code></pre>`
      } catch (__) {}

    return `<pre><code class="language-${lang}">${md.utils.escapeHtml(str)}</code></pre>`
}

function uniqueSlug(slug, slugs) {
    // Mark this slug as used in the environment.
    slugs[slug] = slugs[slug] || 1

    // First slug, return as is.
    if (slugs[slug] === 1) {
      return slug
    }

    // Duplicate slug, add a `-2`, `-3`, etc. to keep ID unique.
    return slug + '-' + slugs[slug]
}

function findLinkOpen(tokens, idx) {
    for (let i = idx - 1; i >= 0; i--)
        if (tokens[i].type == 'link_open') return tokens[i]
}

function renderToken(tokens, idx, options, env, self) {
    return self.renderToken(tokens, idx, options)
}