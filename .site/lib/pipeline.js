const path = require('path')
const { Middleware, ExtensionMiddleware } = require('firesmith/pipe/middleware')
const { md, toc: tocDataSymbol } = require('./markdown')

const baseURL = '//aleph-project.gitlab.io/docs/'
const gitlab = 'https://gitlab.com/'

class DefaultsPostProcessMiddleware extends Middleware {
    async invoke(rsc, next) {
        if ('CI' in process.env)
            rsc.content._base = baseURL

        rsc.content._edit = { path: `${gitlab}aleph-project/docs/blob/master/` }

        if (rsc.content.nav)
            rsc.content.nav._root = rsc.path.replace(/\/_defaults$/, '/')

        return await next(rsc)
    }
}

class ContentPreRenderMiddleware extends Middleware {
    async invoke(rsc, next) {
        if (rsc.data._edit) {
            let name = rsc.name
            if (rsc.data._edit.base) name = path.relative(rsc.data._edit.base, name).replace('\\', '/')
            rsc.data._edit = rsc.data._edit.path + name
        }
        return await next(rsc)
    }
}

class ContentRenderMarkdownMiddleware extends ExtensionMiddleware {
    constructor() {
        super(['.md', '.markdown'])
    }

    async passed(rsc, next) {
        const env = {}
        const tokens = md.parse(rsc.content, env)
        const toc = createToc(rsc, env, tokens)

        if (toc) rsc.data.toc = toc
        rsc.content = md.renderer.render(tokens, md.options, env)
        return await next(rsc)
    }
}

module.exports = {
    DefaultsPostProcessMiddleware,
    ContentPreRenderMiddleware,
    ContentRenderMarkdownMiddleware
}

function createToc(rsc, env, tokens) {
    let maxLevel = 2

    if (rsc.data.toc === false) return
    if (typeof rsc.data.toc === 'number') {
        maxLevel = rsc.data.toc
        if (maxLevel < 1) return
        delete rsc.data.toc
    }
    if (rsc.data.toc !== void 0) return

    const toc = []
    const stack = []

    for (const token of tokens) {
        if (token.type != 'heading_open') continue

        const { level, tocText, anchor } = token[tocDataSymbol]
        if (level > maxLevel) continue

        const entry = { level, children: [], text: tocText, link: anchor }

        while (stack.length > 0 && stack[0].level >= level) {
            stack.shift()
        }

        if (stack.length == 0)
            toc.push(entry)
        else
            stack[0].children.push(entry)

        stack.unshift(entry)
    }

    if (!toc.length) return
    if (toc.length == 1 && !toc[0].children.length) return
    return toc
}