# Pages Skeleton

This is a GitLab Pages project skeleton.

## Getting Started

You must have NodeJS installed to locally test the website generation.

To create your own project, copy this one and update `.site/package.json` to reflect the correct `name` and `author`. Do not modify any dependencies, or you will likely break the build process.

Before building, run `cd .site; npm install`. To test the site locally, run `node .site/serve.js` and navigate to http://localhost:1337. The Node script will rebuild the website whenever you update a file. To generate the website, run `node .site/build.js`. This will run once and complete, without running a webserver or rebuilding when you update a file.

## Contributing

### Global Configuration

A defaults file (`_defaults.yml`) in a directory affects all documents in that directory and any subdirectories. The defaults file in the root of the project defines the global defaults and affects the entire project.

#### Title

To change the title that is displayed in the header of every page, update `global` / `title`.

#### Menu

To change the menu that is displayed in the header of every page, update `global` / `menu`. The name of each entry (e.g. `help`) **MUST** refer to a project directory and MUST be identical to that directory's name. The `title` of an entry (e.g. `Help`) is the text of the link displayed in the header. The `icon` of an entry (e.g. `question-circle`) is a [Font Awesome](https://fontawesome.com/) icon. To choose an icon, look up an icon on [Font Awesome's gallery](https://fontawesome.com/icons?d=gallery) and use the name of the icon as the value for `icon`.

### Navigation

Each section (directory) can have it's own navigation, defined in the defaults file for that directory. This is optional. Technically, every page can have its own navigation, but that is unlikely to be useful.

Navigation is defined in the defaults file by the `nav:` value and its subsections. Each subsection must have a title (e.g. `Help` or `More` in the `help` directory). Within a subsection, navigation links are specified as `Title: page.html` (e.g. `Topics: topics.html`). The first part is the title, and the second part refers to a document. All `page.md` documents will be compiled into `page.html`. Thus, to link to `page.md`, the navigation entry must specify `page.html`. Using the special value of `~` or `null` indicates that entry should refer to the index page for that directory.

### Writing Documents

Documents should be written in [Markdown](https://commonmark.org/help/), using the CommonMark standard. The beginning of each Markdown document should include [YAML front matter](http://assemble.io/docs/YAML-front-matter.html), which at the very least should specify the page title.

Minimal front matter:

```yaml
---
title: My New Document
---
```

Other supported values:
```yaml
---
_layout: bare
title: My New Document
author: Some Dude
toc: false
---
```

The layout (`_layout`) can be `bare` or `main`. The default is `main`, as specified in the root defaults file. `bare` is `main` without the section navigation elements. The default `author` is specified in the root defaults file, but can be overridden for a page. The table of contents generator can be disabled by setting `toc` to `false`.